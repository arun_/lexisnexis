# lexisnexis

**Please access the application using below URL**

**http://34.200.223.167:5000/**



```
For temp i commented terraform stage from deployment section but terrafom code available in same repo main branch 
```


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/arun_/lexisnexis.git
git branch -M main
git push -uf origin main
```


## Description

This is a simple python application deploying to an EC2 instnace 

**NOTE** 
Now this application or infrastructure is not running when ever we need that time we can deploy for the first time 

This Pipeline have four stages **"Build, Test, Terraform_deploy and Deploy"**

First time only need teffaform_deploy stage next time onwards we can comment this stage in gitlab-ci.yml file section 

**Application listening on port 5000**

So the public URL we can access **http://public-ip-ec2:5000**

**The IP Address of the EC2 will get from the deploy stage console output**

## Usage

**First time deployment will create infrastructure VPC and EC2**

If you need to deploy any new feature to this application in future then please comment/remove terraform_deploy stage  and update deploy stage with newly created ec2 public IP

Avoid terrform_deployment stage second deployment onwards or else this will create new infrastructure and deploy application there 


## Contributing

