from flask import Flask, request
import datetime

app = Flask(__name__)

@app.route('/')
def test_hello():
    source_ip = request.remote_addr
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return f"LexisNexis test for Arun!\nYour Source IP: {source_ip}\nCurrent Time: {current_time}"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
