from flask import Flask
import datetime

app = Flask(__name__)

@app.route('/')
def test_hello():
    #current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return f"LexisNexis test for Arun!   RiskNarrative devops-exercise"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)