provider "aws" {
  region = "us-east-1"  # Update with your desired AWS region
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]  # Adjust the AZs based on your desired region
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.4.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24", "10.0.104.0/24"]
#  db_subnets      = ["10.0.201.0/24", "10.0.202.0/24"]

  enable_nat_gateway   = false
  single_nat_gateway   = false
  reuse_nat_ips        = false
  enable_vpn_gateway   = false
  enable_dns_hostnames = true

  tags = {
    "Environment" = "production"
    "Project"     = "my-project"
  }
}

resource "aws_instance" "app_server" {
  ami           =  "ami-0261755bbcb8c4a84"
  instance_type = "t2.micro"
  key_name      = "lexisnexis"
  subnet_id     = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.app_server_sg.id]  # Associate the security group with the EC2 instance
  associate_public_ip_address = true
  
  root_block_device {
    volume_size = 10
  }
  
  user_data = <<-EOF
    #!/bin/bash
    sudo apt update -y
    sudo apt install python3 -y
    sudo apt install python3-pip -y
    sudo pip install flask
    EOF
}

resource "aws_security_group" "app_server_sg" {
  name        = "app-server-sg"
  description = "Security group for the app server"

  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Output the public IP address of the EC2 instance
output "instance_public_ip" {
  value = aws_instance.app_server.public_ip
}
